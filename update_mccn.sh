#!/bin/bash

set -x

cd /opt/capnet
sudo git pull
sudo git checkout dist-capnet

cd ../capnet.obj
sudo ../capnet/configure --with-protoc=/usr/local --with-libcap=/usr/local --with-mul=/usr/local
sudo make
