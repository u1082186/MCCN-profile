"""
Multi-Cloud capnet profile.
Initializes a topology with 2 nodes connected with static links to each other using a gateway node.
The number of nodes is parameterized and can be changed while instantiating.

Instructions:
If you use the default OS image for the nodes, look in /opt for the cloned capnet repo, and other dependent repos.
All dependencies are installed in /usr/local.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

logFilePath = "/tmp/CloudLabSetupScripts.log"
SETUP_CMD = "sudo sh /local/repository/setup_permissions.sh 2>&1 | sudo tee -a " + logFilePath 
MCCN_UPDATE_CMD = "sudo sh /local/repository/update_mccn.sh 2>&1 | sudo tee -a " + logFilePath

DEFAULT_NUM_NODES = 2
DIST_CAPNET_IMG = 'urn:publicid:IDN+emulab.net+image+TCloud:dist-capnet'
UBUNTUx64_1604_LTS_IMG =  'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'

IPPREFIX = "192.168."
NETMASK = "255.255.255.0"
#use the last two valid IPs=253,254 in /24 for the physical interface and router.
GATEWAY_IP = ".254"
NODE_PHYSICAL_IP = ".253"

NODE_NAME_PREFIX = "n"
IFACE_NAME_PREFIX = "eth"

# Create a portal object,
pc = portal.Context()

#define parameters
pc.defineParameter("numNodes","Number of MCCN Controllers",
                   portal.ParameterType.INTEGER, DEFAULT_NUM_NODES,
                   longDescription="Provide the number of Controller/Cloud nodes. Each node is a self contained cloud, with a MCCN Controller. The user VMs/machines on each cloud may be manifested as different network namespaces using the capnet/etc/ovs-ns-net.sh script")
pc.defineParameter("nodeImg", "The OS image to put on the nodes that host the clouds",
                    portal.ParameterType.STRING, DIST_CAPNET_IMG, advanced=True,
                    longDescription="The default image contains the required pre-requisites for MCCN installed under /usr/local, e.g. openmul, libcap, protobuf, protobuf-c, json-c, etc. It also has the libcap,openMUL and capnet repos checked out under /opt")
pc.defineParameter("gatewayImg", "The OS image to put on the router/gateway node",
                    portal.ParameterType.STRING, UBUNTUx64_1604_LTS_IMG, advanced=True)   
pc.defineParameter("ipprefix", "Please provide the first 2 octets only. example, 10.0 or 192.168",
                    portal.ParameterType.STRING, IPPREFIX,
                    longDescription="The IP addresses assigned to all interfaces are /24 addresses. The IPPrefix given here will only provide the first 2 octets of the IP address. The third octet will be decided by the script based on node number, i.e., IPPrefix.1 for node1, IPPrefix.2 for node2, etc. The 4th octect will be sequentially provided by the script.")   

params = pc.bindParameters()

#parameter sanity
if params.numNodes < 1:
    perr = portal.ParameterError("Must have atleast 1 Controller node.")
    pc.reportError(perr)

if params.numNodes > 10:
    perr = portal.ParameterError("Why do you need so many Controllers/Clouds? Choose a number less than or equal to 10.")
    pc.reportError(perr)

if not params.nodeImg:
    pwarn = portal.ParameterWarning("No OS image to put on the node provided. Using default image.")
    pc.reportWarning(pwarn)
    params.nodeImg = DIST_CAPNET_IMG

if not params.gatewayImg:
    pwarn = portal.ParameterWarning("No OS image to put on gateway node provided. Using default.")
    pc.reportWarning(pwarn)
    params.gatewayImg = UBUNTUx64_1604_LTS_IMG

pc.verifyParameters()

IPPREFIX = params.ipprefix;
if IPPREFIX[-1]!=".":
    IPPREFIX+="."

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

linkedIfaces = dict()
for i in range(params.numNodes):
    nodeNumber = i+1
    nodeName = NODE_NAME_PREFIX + str(nodeNumber) 
    node = request.RawPC(nodeName)
    node.disk_image = params.nodeImg
    ipv4 = IPPREFIX + str(nodeNumber) + NODE_PHYSICAL_IP
    ifaceName = IFACE_NAME_PREFIX + "0"
    iface = node.addInterface(ifaceName, pg.IPv4Address(ipv4, NETMASK))
    # Install and execute a script that is contained in the repository.
    node.addService(pg.Execute(shell="sh", command=MCCN_UPDATE_CMD))
    node.addService(pg.Execute(shell="sh", command=SETUP_CMD))
    linkedIfaces[nodeName] = [iface]

# Node gw
node_gw = request.RawPC('gw')
node_gw.disk_image = params.gatewayImg
#add an interface per node
for i in range(params.numNodes):
    nodeNumber = i+1
    nodeName = NODE_NAME_PREFIX + str(nodeNumber) 
    ifaceName = IFACE_NAME_PREFIX + str(nodeNumber)
    ipv4 = IPPREFIX + str(nodeNumber) + GATEWAY_IP 
    iface_gw = node_gw.addInterface(ifaceName, pg.IPv4Address(ipv4, NETMASK))
    linkedIfaces[nodeName].append(iface_gw)

# Add all static Links
for nodeName,ifaces in linkedIfaces.items():
    linkName = "gw-" + nodeName + "-link"
    link = request.Link(linkName)
    link.trivial_ok = True
    link.addInterface(ifaces[0])
    link.addInterface(ifaces[1])

request.setRoutingStyle('static')

# Print the generated rspec
pc.printRequestRSpec(request)
