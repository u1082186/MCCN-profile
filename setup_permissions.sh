#!/bin/bash

set -x

sudo groupadd capnet
sudo usermod vatsa -a -G capnet
sudo chgrp -R capnet /opt
sudo chmod -R g+w /opt
