# MCCN-profile

Multi-Cloud Capability Network Cloudlab profile.

Initializes a topology with 3 nodes connected with static links to each other using a gateway node.

                   ----         ----         ----
       10.1.1.2/24 |n1|---------|gw|---------|n2| 10.1.2.2/24
                   ----         ----         ----
                                 |
                                 |
                                 |
                                 |
                                ----
                                |n3| 10.1.3.2/24
                                ----

Uses an Ubuntu 16.04 based image on all nodes that has *capnet* cloned on them in path /opt.
Dependencies of *capnet* are already installed.
